/*
 * Globais.h
 *
 *  Created on: 2 de ago de 2020
 *      Author: Marcel Giraldi
 *
 */

#ifndef INC_GLOBAIS_H_
#define INC_GLOBAIS_H_

#define global

#include <stdio.h>
#include "Defines.h"

typedef unsigned char byte;

global const float SCALA_G[4]
#ifdef DEF_VAR
={ 0.061, 0.488, 0.122, 0.244}
#endif
;

global uint32_t UID_Word0;
global uint32_t UID_Word1;
global uint32_t UID_Word2;

global int16_t data_acceleration[QTDE_PONTOS_MATRIZ];
global int16_t data_raw_acceleration[QTDE_PONTOS_MATRIZ];
global int16_t accel;

global uint8_t pacotes_tx[NUM_DE_PACOTES+6];
global uint8_t rx_buffer[10][BYTES_TX+2];
global uint8_t tx_buffer[NUM_DE_PACOTES+6][BYTES_TX+2];

global uint8_t Flag_Configuracao;
global uint8_t recebendo_Radio;
global uint8_t RF_Recepcao;

global uint8_t timer_25ms;
global uint16_t time_out_Acc;
global uint16_t time_desprezar;
global uint16_t timeout_RX_Radio;
global uint32_t time_out_RF;
global uint8_t to_RSD_WAIT_TX_FLAG;
global uint32_t tempo_total;
global uint32_t time_1seg;
global uint32_t Delta_Time_s;
global uint32_t tempo_decorrido;
global int32_t Time_processo;
global int16_t time_daley_ms;

global uint32_t Counter_Interrupt1;
global uint32_t Counter_Interrupt2;
global uint32_t transmitindo;

global uint8_t TX_curto;

global uint8_t tttc_conf_Acc;
global uint8_t tttv_conf_RF;
global uint8_t tttv_conf2_RF;
global uint8_t tttv_res_RF;

global uint8_t Estado_Leitura;

global uint8_t iniciou_bat;
global uint8_t Iniciou_rx_val;

global float v_bateria;
global uint32_t AD_Bateria;
global uint32_t AD_Temp_u;
global int16_t P_bateria;

global float v_NTC100k;
global uint32_t AD_NTC100k;
global int16_t T_NTC100k;
global int16_t T_Micro;

global float v_Vbat;
global uint32_t AD_Vbat;

global uint8_t qtde_bytes_RX;

global int32_t I;
global int32_t J;
global int32_t I1;
global uint8_t pos_freq;
global uint8_t baud_rate;
global uint8_t enviar_ultimo;
global uint8_t reset_spirit1;
global int32_t Int_GPIO3;

global uint8_t bytes_pacote;
global uint8_t qual_eixo;
global uint8_t leu_temp;
global uint8_t inicializar_iis3dwb;
global uint8_t case_tx;
global uint16_t num_pacote;
global uint16_t num_pacote2;
global uint16_t num_pacote_aux;
global uint16_t qual_pacote;
global uint16_t escala_g;
global int16_t Valor_Temp;
global int16_t menor_amplitude;
global int16_t maior_amplitude;
global uint16_t step;
global uint32_t pointerToMemory;
global long total_ACC; // usado para média de amplitudes

global SPI_HandleTypeDef SPI_ACC;
global SPI_HandleTypeDef SPI_RF;

global char ID_Sensor_1000[TAMANHO_ID];

/* LLP configuration parameters */

void iis3dwb_read_data_polling (void);

#endif /* INC_GLOBAIS_H_ */

