/*
 * Spirit_Types.h
 *
 *  Created on: 5 de out de 2020
 *      Author: Marcel Giraldi
 */

#ifndef INC_SPIRIT_TYPES_H_
#define INC_SPIRIT_TYPES_H_

typedef enum
{
  MC_STATE_STANDBY           =0x40,	/*!< STANDBY */
  MC_STATE_SLEEP             =0x36,	/*!< SLEEP */
  MC_STATE_READY             =0x03,	/*!< READY */
  MC_STATE_PM_SETUP          =0x3D,	/*!< PM_SETUP */
  MC_STATE_XO_SETTLING       =0x23,	/*!< XO_SETTLING */
  MC_STATE_SYNTH_SETUP       =0x53,	/*!< SYNT_SETUP */
  MC_STATE_PROTOCOL          =0x1F,	/*!< PROTOCOL */
  MC_STATE_SYNTH_CALIBRATION =0x4F,	/*!< SYNTH */
  MC_STATE_LOCK              =0x0F,	/*!< LOCK */
  MC_STATE_RX                =0x33,	/*!< RX */
  MC_STATE_TX                =0x5F	/*!< TX */
} SpiritState;

#endif /* INC_SPIRIT_TYPES_H_ */
