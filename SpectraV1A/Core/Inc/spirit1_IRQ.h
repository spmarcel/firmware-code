/* Includes ------------------------------------------------------------------*/
#include <stdint.h>
#include "stm32l4xx_hal.h"
		
#define	IRQ_MASK3_BASE						((uint8_t)0x90) /*!< IRQ_MASK is split into 4 registers*/		
#define IRQ_STATUS3_BASE					((uint8_t)0xFA) /*!< IRQ Events(RR, split into 4 registers) */

typedef enum
{
  S_RESET = 0,
  S_SET = !S_RESET
} SpiritFlagStatus;		

typedef struct
{
  SpiritFlagStatus  IRQ_RX_DATA_READY:1;            /*!< IRQ: RX data ready */
  SpiritFlagStatus  IRQ_RX_DATA_DISC:1;             /*!< IRQ: RX data discarded (upon filtering) */
  SpiritFlagStatus  IRQ_TX_DATA_SENT:1;             /*!< IRQ: TX data sent */
  SpiritFlagStatus  IRQ_MAX_RE_TX_REACH:1;          /*!< IRQ: Max re-TX reached */
  SpiritFlagStatus  IRQ_CRC_ERROR:1;                /*!< IRQ: CRC error */
  SpiritFlagStatus  IRQ_TX_FIFO_ERROR:1;            /*!< IRQ: TX FIFO underflow/overflow error */
  SpiritFlagStatus  IRQ_RX_FIFO_ERROR:1;            /*!< IRQ: RX FIFO underflow/overflow error */
  SpiritFlagStatus  IRQ_TX_FIFO_ALMOST_FULL:1;      /*!< IRQ: TX FIFO almost full */

  SpiritFlagStatus  IRQ_TX_FIFO_ALMOST_EMPTY:1;     /*!< IRQ: TX FIFO almost empty */
  SpiritFlagStatus  IRQ_RX_FIFO_ALMOST_FULL:1;      /*!< IRQ: RX FIFO almost full */
  SpiritFlagStatus  IRQ_RX_FIFO_ALMOST_EMPTY:1;     /*!< IRQ: RX FIFO almost empty  */
  SpiritFlagStatus  IRQ_MAX_BO_CCA_REACH:1;         /*!< IRQ: Max number of back-off during CCA */
  SpiritFlagStatus  IRQ_VALID_PREAMBLE:1;           /*!< IRQ: Valid preamble detected */
  SpiritFlagStatus  IRQ_VALID_SYNC:1;               /*!< IRQ: Sync word detected */
  SpiritFlagStatus  IRQ_RSSI_ABOVE_TH:1;            /*!< IRQ: RSSI above threshold */
  SpiritFlagStatus  IRQ_WKUP_TOUT_LDC:1;            /*!< IRQ: Wake-up timeout in LDC mode */

  SpiritFlagStatus  IRQ_READY:1;                    /*!< IRQ: READY state */
  SpiritFlagStatus  IRQ_STANDBY_DELAYED:1;          /*!< IRQ: STANDBY state after MCU_CK_CONF_CLOCK_TAIL_X clock cycles */
  SpiritFlagStatus  IRQ_LOW_BATT_LVL:1;             /*!< IRQ: Battery level below threshold*/
  SpiritFlagStatus  IRQ_POR:1;                      /*!< IRQ: Power On Reset */
  SpiritFlagStatus  IRQ_BOR:1;                      /*!< IRQ: Brown out event (both accurate and inaccurate)*/
  SpiritFlagStatus  IRQ_LOCK:1;                     /*!< IRQ: LOCK state */
  SpiritFlagStatus  IRQ_PM_COUNT_EXPIRED:1;         /*!< IRQ: only for debug; Power Management startup timer expiration (see reg PM_START_COUNTER, 0xB5) */
  SpiritFlagStatus  IRQ_XO_COUNT_EXPIRED:1;         /*!< IRQ: only for debug; Crystal oscillator settling time counter expired */

  SpiritFlagStatus  IRQ_SYNTH_LOCK_TIMEOUT:1;       /*!< IRQ: only for debug; LOCK state timeout */
  SpiritFlagStatus  IRQ_SYNTH_LOCK_STARTUP:1;       /*!< IRQ: only for debug; see CALIBR_START_COUNTER */
  SpiritFlagStatus  IRQ_SYNTH_CAL_TIMEOUT:1;        /*!< IRQ: only for debug; SYNTH calibration timeout */
  SpiritFlagStatus  IRQ_TX_START_TIME:1;            /*!< IRQ: only for debug; TX circuitry startup time; see TX_START_COUNTER */
  SpiritFlagStatus  IRQ_RX_START_TIME:1;            /*!< IRQ: only for debug; RX circuitry startup time; see TX_START_COUNTER */
  SpiritFlagStatus  IRQ_RX_TIMEOUT:1;               /*!< IRQ: RX operation timeout */
  SpiritFlagStatus  IRQ_AES_END:1;                  /*!< IRQ: AES End of operation */
  SpiritFlagStatus  :1;                             /*!< Reserved bit */
} SpiritIrqs;

typedef enum
{
  RX_DATA_READY = 0x00000001,           /*!< IRQ: RX data ready */
  RX_DATA_DISC = 0x00000002,            /*!< IRQ: RX data discarded (upon filtering) */
  TX_DATA_SENT = 0x00000004,            /*!< IRQ: TX data sent */
  MAX_RE_TX_REACH = 0x00000008,         /*!< IRQ: Max re-TX reached */
  CRC_ERROR = 0x00000010,               /*!< IRQ: CRC error */
  TX_FIFO_ERROR = 0x00000020,           /*!< IRQ: TX FIFO underflow/overflow error */
  RX_FIFO_ERROR = 0x00000040,           /*!< IRQ: RX FIFO underflow/overflow error */
  TX_FIFO_ALMOST_FULL = 0x00000080,     /*!< IRQ: TX FIFO almost full */
  TX_FIFO_ALMOST_EMPTY = 0x00000100,    /*!< IRQ: TX FIFO almost empty */
  RX_FIFO_ALMOST_FULL = 0x00000200,     /*!< IRQ: RX FIFO almost full */
  RX_FIFO_ALMOST_EMPTY = 0x00000400,    /*!< IRQ: RX FIFO almost empty  */
  MAX_BO_CCA_REACH = 0x00000800,        /*!< IRQ: Max number of back-off during CCA */
  VALID_PREAMBLE = 0x00001000,          /*!< IRQ: Valid preamble detected */
  VALID_SYNC = 0x00002000,              /*!< IRQ: Sync word detected */
  RSSI_ABOVE_TH = 0x00004000,           /*!< IRQ: RSSI above threshold */
  WKUP_TOUT_LDC = 0x00008000,           /*!< IRQ: Wake-up timeout in LDC mode */
  READY = 0x00010000,                   /*!< IRQ: READY state */
  STANDBY_DELAYED = 0x00020000,         /*!< IRQ: STANDBY state after MCU_CK_CONF_CLOCK_TAIL_X clock cycles */
  LOW_BATT_LVL = 0x00040000,            /*!< IRQ: Battery level below threshold*/
  POR = 0x00080000,                     /*!< IRQ: Power On Reset */
  BOR = 0x00100000,                     /*!< IRQ: Brown out event (both accurate and inaccurate)*/
  LOCK = 0x00200000,                    /*!< IRQ: LOCK state */
  PM_COUNT_EXPIRED = 0x00400000,        /*!< IRQ: only for debug; Power Management startup timer expiration (see reg PM_START_COUNTER, 0xB5) */
  XO_COUNT_EXPIRED = 0x00800000,        /*!< IRQ: only for debug; Crystal oscillator settling time counter expired */
  SYNTH_LOCK_TIMEOUT = 0x01000000,      /*!< IRQ: only for debug; LOCK state timeout */
  SYNTH_LOCK_STARTUP = 0x02000000,      /*!< IRQ: only for debug; see CALIBR_START_COUNTER */
  SYNTH_CAL_TIMEOUT = 0x04000000,       /*!< IRQ: only for debug; SYNTH calibration timeout */
  TX_START_TIME = 0x08000000,	        /*!< IRQ: only for debug; TX circuitry startup time; see TX_START_COUNTER */
  RX_START_TIME = 0x10000000,	        /*!< IRQ: only for debug; RX circuitry startup time; see TX_START_COUNTER */
  RX_TIMEOUT = 0x20000000,	        /*!< IRQ: RX operation timeout */
  AES_END = 0x40000000,                 /*!< IRQ: AES End of operation */
  ALL_IRQ = 0x7FFFFFFF			/*!< All the above mentioned IRQs */
} IrqList;

typedef enum {
  S_DISABLE = 0,
  S_ENABLE = !S_DISABLE
} SFunctionalState;

typedef enum
{
	FSK         = 0x00, /*!< 2-FSK modulation selected */
	GFSK_BT05   = 0x50, /*!< GFSK modulation selected with BT=0.5 */
	GFSK_BT1    = 0x10, /*!< GFSK modulation selected with BT=1 */
	ASK_OOK     = 0x20, /*!< ASK or OOK modulation selected. ASK will use power ramping */
	MSK         = 0x30  /*!< MSK modulation selected */
} ModulationSelect;

typedef struct
{
	int16_t           nXtalOffsetPpm;     /*!< Specifies the offset frequency (in ppm)
											 to compensate crystal inaccuracy expressed
											 as signed value.*/
	uint32_t          lFrequencyBase;     /*!< Specifies the base carrier frequency (in Hz),
											 i.e. the carrier frequency of channel #0.
											 This parameter can be in one of the following ranges:
											 High_Band: from 779 MHz to 915 MHz
											 Middle Band: from 387 MHz to 470 MHz
											 Low Band: from 300 MHz to 348 MHz */
	uint32_t          nChannelSpace;      /*!< Specifies the channel spacing expressed in Hz.
											 The channel spacing is expressed as:
											 NxFREQUENCY_STEPS, where FREQUENCY STEPS
											 is F_Xo/2^15.
											 This parameter can be in the range: [0, F_Xo/2^15*255] Hz */
	uint8_t           cChannelNumber;      /*!< Specifies the channel number. This value
											 is multiplied by the channel spacing and
											 added to synthesizer base frequency to
											 generate the actual RF carrier frequency */
	ModulationSelect  xModulationSelect;   /*!< Specifies the modulation. This
											 parameter can be any value of
											 @ref ModulationSelect */
	uint32_t          lDatarate;          /*!< Specifies the datarate expressed in bps.
											 This parameter can be in the range between
											 100 bps and 500 kbps */
	uint32_t          lFreqDev;           /*!< Specifies the frequency deviation expressed in Hz.
											 This parameter can be in the range: [F_Xo*8/2^18, F_Xo*7680/2^18] Hz */
	uint32_t          lBandwidth;          /*!< Specifies the channel filter bandwidth
											 expressed in Hz. This parameter can be
											 in the range between 1100 and 800100 Hz */
} SRadioInit;

#define WAKEUP_TIMER                100.0

#define GPIO3_CONF_BASE						((uint8_t)0x02) /*!< GPIO_3 register address */
#define GPIO2_CONF_BASE						((uint8_t)0x03) /*!< GPIO_3 register address */
#define GPIO1_CONF_BASE						((uint8_t)0x04) /*!< GPIO_3 register address */
#define GPIO0_CONF_BASE						((uint8_t)0x05) /*!< GPIO_3 register address */

typedef enum
{
	PKT_PREAMBLE_LENGTH_01BYTE            = 0x00, /*!< Preamble length 1 byte*/
	PKT_PREAMBLE_LENGTH_02BYTES           = 0x08, /*!< Preamble length 2 bytes */
	PKT_PREAMBLE_LENGTH_03BYTES           = 0x10, /*!< Preamble length 3 bytes */
	PKT_PREAMBLE_LENGTH_04BYTES           = 0x18, /*!< Preamble length 4 bytes */
	PKT_PREAMBLE_LENGTH_05BYTES           = 0x20, /*!< Preamble length 5 bytes */
	PKT_PREAMBLE_LENGTH_06BYTES           = 0x28, /*!< Preamble length 6 bytes */
	PKT_PREAMBLE_LENGTH_07BYTES           = 0x30, /*!< Preamble length 7 bytes */
	PKT_PREAMBLE_LENGTH_08BYTES           = 0x38, /*!< Preamble length 8 bytes */
	PKT_PREAMBLE_LENGTH_09BYTES           = 0x40, /*!< Preamble length 9 bytes */
	PKT_PREAMBLE_LENGTH_10BYTES           = 0x48, /*!< Preamble length 10 bytes */
	PKT_PREAMBLE_LENGTH_11BYTES           = 0x50, /*!< Preamble length 11 bytes */
	PKT_PREAMBLE_LENGTH_12BYTES           = 0x58, /*!< Preamble length 12 bytes */
	PKT_PREAMBLE_LENGTH_13BYTES           = 0x60, /*!< Preamble length 13 bytes */
	PKT_PREAMBLE_LENGTH_14BYTES           = 0x68, /*!< Preamble length 14 bytes */
	PKT_PREAMBLE_LENGTH_15BYTES           = 0x70, /*!< Preamble length 15 bytes */
	PKT_PREAMBLE_LENGTH_16BYTES           = 0x78, /*!< Preamble length 16 bytes */
	PKT_PREAMBLE_LENGTH_17BYTES           = 0x80, /*!< Preamble length 17 bytes */
	PKT_PREAMBLE_LENGTH_18BYTES           = 0x88, /*!< Preamble length 18 bytes */
	PKT_PREAMBLE_LENGTH_19BYTES           = 0x90, /*!< Preamble length 19 bytes */
	PKT_PREAMBLE_LENGTH_20BYTES           = 0x98, /*!< Preamble length 20 bytes */
	PKT_PREAMBLE_LENGTH_21BYTES           = 0xA0, /*!< Preamble length 21 bytes */
	PKT_PREAMBLE_LENGTH_22BYTES           = 0xA8, /*!< Preamble length 22 bytes */
	PKT_PREAMBLE_LENGTH_23BYTES           = 0xB0, /*!< Preamble length 23 bytes */
	PKT_PREAMBLE_LENGTH_24BYTES           = 0xB8, /*!< Preamble length 24 bytes */
	PKT_PREAMBLE_LENGTH_25BYTES           = 0xC0, /*!< Preamble length 25 bytes */
	PKT_PREAMBLE_LENGTH_26BYTES           = 0xC8, /*!< Preamble length 26 bytes */
	PKT_PREAMBLE_LENGTH_27BYTES           = 0xD0, /*!< Preamble length 27 bytes */
	PKT_PREAMBLE_LENGTH_28BYTES           = 0xD8, /*!< Preamble length 28 bytes */
	PKT_PREAMBLE_LENGTH_29BYTES           = 0xE0, /*!< Preamble length 29 bytes */
	PKT_PREAMBLE_LENGTH_30BYTES           = 0xE8, /*!< Preamble length 30 bytes */
	PKT_PREAMBLE_LENGTH_31BYTES           = 0xF0, /*!< Preamble length 31 bytes */
	PKT_PREAMBLE_LENGTH_32BYTES           = 0xF8  /*!< Preamble length 32 bytes */
} PktPreambleLength;

typedef enum
{
	PKT_SYNC_LENGTH_1BYTE            = 0x00, /*!< Sync length 1 byte*/
	PKT_SYNC_LENGTH_2BYTES           = 0x02, /*!< Sync length 2 bytes*/
	PKT_SYNC_LENGTH_3BYTES           = 0x04, /*!< Sync length 3 bytes */
	PKT_SYNC_LENGTH_4BYTES           = 0x06 , /*!< Sync length 4 bytes */
} PktSyncLength;

typedef enum
{
	PKT_NO_CRC               = 0x00, /*!< No CRC                              */
	PKT_CRC_MODE_8BITS       = 0x20, /*!< CRC length 8 bits  - poly: 0x07     */
	PKT_CRC_MODE_16BITS_1    = 0x40, /*!< CRC length 16 bits - poly: 0x8005   */
	PKT_CRC_MODE_16BITS_2    = 0x60, /*!< CRC length 16 bits - poly: 0x1021   */
	PKT_CRC_MODE_24BITS      = 0x80, /*!< CRC length 24 bits - poly: 0x864CFB */
} PktCrcMode;

typedef enum
{
	PKT_LENGTH_FIX  = 0x00,    /*!< Fixed payload length     */
	PKT_LENGTH_VAR  = 0x01     /*!< Variable payload length  */
} PktFixVarLength;

typedef enum
{
	PKT_CONTROL_LENGTH_0BYTES = 0x00,     /*!< Control length 0 byte*/
	PKT_CONTROL_LENGTH_1BYTE,             /*!< Control length 1 byte*/
	PKT_CONTROL_LENGTH_2BYTES,            /*!< Control length 2 bytes*/
	PKT_CONTROL_LENGTH_3BYTES,            /*!< Control length 3 bytes*/
	PKT_CONTROL_LENGTH_4BYTES             /*!< Control length 4 bytes*/
} PktControlLength;

typedef enum
{
	PKT_SYNC_WORD_1=0x01,  /*!< Index of the 1st sync word*/
	PKT_SYNC_WORD_2,       /*!< Index of the 2nd sync word*/
	PKT_SYNC_WORD_3,       /*!< Index of the 3rd sync word*/
	PKT_SYNC_WORD_4        /*!< Index of the 4th sync word*/
} PktSyncX;

typedef enum
{
	SPIRIT_GPIO_0  = GPIO0_CONF_BASE, /*!< GPIO_0 selected */
	SPIRIT_GPIO_1  = GPIO1_CONF_BASE, /*!< GPIO_1 selected */
	SPIRIT_GPIO_2  = GPIO2_CONF_BASE, /*!< GPIO_2 selected */
	SPIRIT_GPIO_3  = GPIO3_CONF_BASE  /*!< GPIO_3 selected */
} SpiritGpioPin;

typedef enum
{
	SPIRIT_GPIO_MODE_DIGITAL_INPUT      = 0x01, /*!< Digital Input on GPIO */
	SPIRIT_GPIO_MODE_DIGITAL_OUTPUT_LP  = 0x02, /*!< Digital Output on GPIO (low current) */
	SPIRIT_GPIO_MODE_DIGITAL_OUTPUT_HP  = 0x03  /*!< Digital Output on GPIO (high current) */
} SpiritGpioMode;

typedef enum
{
	SPIRIT_GPIO_DIG_OUT_IRQ                               = 0x00, /*!< nIRQ (Interrupt Request, active low) , default configuration after POR */
	SPIRIT_GPIO_DIG_OUT_POR_INV                           = 0x08, /*!< POR inverted (active low) */
	SPIRIT_GPIO_DIG_OUT_WUT_EXP                           = 0x10, /*!< Wake-Up Timer expiration: "1" when WUT has expired */
	SPIRIT_GPIO_DIG_OUT_LBD                               = 0x18, /*!< Low battery detection: "1" when battery is below threshold setting */
	SPIRIT_GPIO_DIG_OUT_TX_DATA                           = 0x20, /*!< TX data internal clock output (TX data are sampled on the rising edge of it) */
	SPIRIT_GPIO_DIG_OUT_TX_STATE                          = 0x28, /*!< TX state indication: "1" when Spirit1 is passing in the TX state */
	SPIRIT_GPIO_DIG_OUT_TX_FIFO_ALMOST_EMPTY              = 0x30, /*!< TX FIFO Almost Empty Flag */
	SPIRIT_GPIO_DIG_OUT_TX_FIFO_ALMOST_FULL               = 0x38, /*!< TX FIFO Almost Full Flag */
	SPIRIT_GPIO_DIG_OUT_RX_DATA                           = 0x40, /*!< RX data output */
	SPIRIT_GPIO_DIG_OUT_RX_CLOCK                          = 0x48, /*!< RX clock output (recovered from received data) */
	SPIRIT_GPIO_DIG_OUT_RX_STATE                          = 0x50, /*!< RX state indication: "1" when Spirit1 is passing in the RX state */
	SPIRIT_GPIO_DIG_OUT_RX_FIFO_ALMOST_FULL               = 0x58, /*!< RX FIFO Almost Full Flag */
	SPIRIT_GPIO_DIG_OUT_RX_FIFO_ALMOST_EMPTY              = 0x60, /*!< RX FIFO Almost Empty Flag */
	SPIRIT_GPIO_DIG_OUT_ANTENNA_SWITCH                    = 0x68, /*!< Antenna switch used for antenna diversity  */
	SPIRIT_GPIO_DIG_OUT_VALID_PREAMBLE                    = 0x70, /*!< Valid Preamble Detected Flag */
	SPIRIT_GPIO_DIG_OUT_SYNC_DETECTED                     = 0x78, /*!< Sync WordSync Word Detected Flag */
	SPIRIT_GPIO_DIG_OUT_RSSI_THRESHOLD                    = 0x80, /*!< RSSI above threshold */
	SPIRIT_GPIO_DIG_OUT_MCU_CLOCK                         = 0x88, /*!< MCU Clock */
	SPIRIT_GPIO_DIG_OUT_TX_RX_MODE                        = 0x90, /*!< TX or RX mode indicator (to enable an external range extender) */
	SPIRIT_GPIO_DIG_OUT_VDD                               = 0x98, /*!< VDD (to emulate an additional GPIO of the MCU, programmable by SPI) */
	SPIRIT_GPIO_DIG_OUT_GND                               = 0xA0, /*!< GND (to emulate an additional GPIO of the MCU, programmable by SPI) */
	SPIRIT_GPIO_DIG_OUT_SMPS_EXT                          = 0xA8, /*!< External SMPS enable signal (active high) */
	SPIRIT_GPIO_DIG_OUT_SLEEP_OR_STANDBY                  = 0xB0,
	SPIRIT_GPIO_DIG_OUT_READY                             = 0xB8,
	SPIRIT_GPIO_DIG_OUT_LOCK                              = 0xC0,
	SPIRIT_GPIO_DIG_OUT_WAIT_FOR_LOCK_SIG                 = 0xC8,
	SPIRIT_GPIO_DIG_OUT_WAIT_FOR_TIMER_FOR_LOCK           = 0xD0,
	SPIRIT_GPIO_DIG_OUT_WAIT_FOR_READY2_SIG               = 0xD8,
	SPIRIT_GPIO_DIG_OUT_WAIT_FOR_TIMER_FOR_PM_SET         = 0xE0,
	SPIRIT_GPIO_DIG_OUT_WAIT_VCO_CALIBRATION              = 0xE8,
	SPIRIT_GPIO_DIG_OUT_ENABLE_SYNTH_FULL_CIRCUIT         = 0xF0,
	SPIRIT_GPIO_DIG_OUT_WAIT_FOR_RCCAL_OK_SIG             = 0xFF,
	SPIRIT_GPIO_DIG_IN_TX_COMMAND                         = 0x00,
	SPIRIT_GPIO_DIG_IN_RX_COMMAND                         = 0x08,
	SPIRIT_GPIO_DIG_IN_TX_DATA_INPUT_FOR_DIRECTRF         = 0x10,
	SPIRIT_GPIO_DIG_IN_DATA_WAKEUP                        = 0x18,
	SPIRIT_GPIO_DIG_IN_EXT_CLOCK_AT_34_7KHZ               = 0x20
} SpiritGpioIO;

typedef PktPreambleLength	BasicPreambleLength;
typedef PktSyncLength       BasicSyncLength;
typedef PktCrcMode          BasicCrcMode;
typedef PktFixVarLength     BasicFixVarLength;
typedef PktControlLength    BasicControlLength;
typedef PktSyncX            BasicSyncX;

typedef struct
{
	BasicPreambleLength           xPreambleLength;        /*!< Specifies the preamble length.
															 This parameter can be any value of @ref BasicPreambleLength */
	BasicSyncLength               xSyncLength;            /*!< Specifies the sync word length. The 32bit word passed (lSyncWords) will be stored in the SYNCx registers from the MSb
															 until the number of bytes in xSyncLength has been stored.
															 This parameter can be any value of @ref BasicSyncLength */
	uint32_t                      lSyncWords;             /*!< Specifies the sync words.
															 This parameter is a uint32_t word with format: 0x|SYNC1|SYNC2|SYNC3|SYNC4| */
	BasicFixVarLength             xFixVarLength;          /*!< Specifies if a fixed length of packet has to be used.
															 This parameter can be any value of @ref BasicFixVarLength */
	uint8_t                       cPktLengthWidth;        /*!< Specifies the size of the length of packet in bits. This field is useful only if
															 the field xFixVarLength is set to BASIC_LENGTH_VAR. For Basic packets the length width
															 is log2( max payload length + control length (0 to 4) + address length (0 or 1)).
															 This parameter is an uint8_t */
	BasicCrcMode                  xCrcMode;               /*!< Specifies the CRC word length of packet.
															 This parameter can be any value of @ref BasicCrcMode */
	BasicControlLength            xControlLength;         /*!< Specifies the length of a control field to be sent.
															 This parameter can be any value of @ref BasicControlLength */
	SFunctionalState         xAddressField;          /*!< Specifies if the destination address has to be sent.
															 This parameter can be S_ENABLE or S_DISABLE */
	SFunctionalState         xFec;                   /*!< Specifies if FEC has to be enabled.
															 This parameter can be S_ENABLE or S_DISABLE */
	SFunctionalState         xDataWhitening;         /*!< Specifies if data whitening has to be enabled.
															 This parameter can be S_ENABLE or S_DISABLE */
} PktBasicInit;

typedef struct
{
	SFunctionalState         xFilterOnMyAddress;             /*!< If set RX packet is accepted if its destination address matches with cMyAddress.
																	 This parameter can be S_ENABLE or S_DISABLE */
	uint8_t                       cMyAddress;                     /*!< Specifies the TX packet source address (address of this node).
																	 This parameter is an uint8_t */
	SFunctionalState         xFilterOnMulticastAddress;      /*!< If set RX packet is accepted if its destination address matches with cMulticastAddress.
																	 This parameter can be S_ENABLE or S_DISABLE */
	uint8_t                       cMulticastAddress;              /*!< Specifies the Multicast group address for this node.
																	 This parameter is an uint8_t */
	SFunctionalState         xFilterOnBroadcastAddress;      /*!< If set RX packet is accepted if its destination address matches with cBroadcastAddress.
																	 This parameter can be S_ENABLE or S_DISABLE */
	uint8_t                       cBroadcastAddress;              /*!< Specifies the Broadcast address for this node.
																	 This parameter is an uint8_t */
} PktBasicAddressesInit;

typedef struct
{
	SpiritGpioPin	xSpiritGpioPin;    /*!< Specifies the GPIO pins to be configured.
										This parameter can be any value of @ref SpiritGpioPin */

	SpiritGpioMode	xSpiritGpioMode;  /*!< Specifies the operating mode for the selected pins.
										This parameter can be a value of @ref SpiritGpioMode */

	SpiritGpioIO	xSpiritGpioIO;      /*!< Specifies the I/O selection for the selected pins.
										This parameter can be a value of @ref SpiritGpioIO */
} SGpioInit;

extern HAL_StatusTypeDef Spirit1GpioIrqDeInit (void);
extern HAL_StatusTypeDef Spirit1GpioIrqGetStatus (SpiritIrqs* pxIrqStatus);
extern HAL_StatusTypeDef Spirit1GpioIrqClearStatus (void);
extern void SpiritGpioInit (SGpioInit* pxGpioInitStruct);
extern HAL_StatusTypeDef Spirit1GpioIrqConfig (IrqList xIrq, SFunctionalState xNewState);
extern void SpiritCsma (SFunctionalState xNewState);


